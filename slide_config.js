var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    //title: 'Android 開發環境建置',
    //subtitle: '工欲善其事，必先利其器',
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    usePrettify: true, // Default: true
    enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
    enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
    //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
    favIcon: 'images/google_developers_logo_tiny.png',
    fonts: [
      'Open Sans:regular,semibold,italic,italicsemibold',
      'Inconsolata'
    ],
    //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
  },

  // Author information
  presenters: [{
    name: 'Qing-Cheng Li',
    company: 'NTU CSIE B97501046/R01922024',
    gplus: 'http://gplus.to/qcl',
    twitter: '@qingchengli',
    www: 'http://qcl.tw/',
    github: 'http://github.com/qcl',
    ptt: 'qcl',
    ptt2: 'qcl'
  }/*, {
    name: 'Second Name',
    company: 'Job Title, Google',
    gplus: 'http://plus.google.com/1234567890',
    twitter: '@yourhandle',
    www: 'http://www.you.com',
    github: 'http://github.com/you'
  }*/]
};

